#include "Settings.h"

#include <QFileInfo>
#include <QApplication>

#include <autoapp/Helpers/ScriptHelper.h>

Settings::Settings() : AbstractSettings(QApplication::applicationDirPath() + "/config.ini")
{
#ifdef __arm__
    set_showCursor(false);
#endif
}

bool Settings::hotSpotEnabled() const
{
    //TODO move it to some SystemManager with threading
    return ScriptHelper::hotSpotEnabled();
}

bool Settings::enableHotSpot() const
{
    //TODO move it to some SystemManager with threading
    return ScriptHelper::enableHotSpot();
}

bool Settings::disableHotSpot() const
{
    //TODO move it to some SystemManager with threading
    return ScriptHelper::disableHotSpot();
}
