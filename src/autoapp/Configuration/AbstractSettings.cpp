#include "AbstractSettings.h"

#include <QCoreApplication>

AbstractSettings::AbstractSettings(const QString &filePath)
{
    m_settings = new QSettings(filePath, QSettings::IniFormat);
}
