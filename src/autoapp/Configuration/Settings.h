#ifndef SETTINGS_H
#define SETTINGS_H

#include "AbstractSettings.h"
#include "../Helpers/PropertyMacros.h"
#include "../Helpers/QObjectHelper.h"

/*!
 * \brief The Settings class - main handler of the all settings shared across the whole application.
 * Class Settings is thread-safe - you can safely use it across all of the threads
 */
class Settings : public AbstractSettings
{
    Q_OBJECT

    SETTING(QStringList, wifiDevices, QStringList())
    SETTING(bool, wifiAutoconnect, true)
    SETTING(int, brightness, 100) //screen brightness in %
    SETTING(int, volume, 100) //volume in %
    SETTING(bool, showCursor, true)

public:
    SINGLETON_BY_VALUE(Settings)

    Q_INVOKABLE bool hotSpotEnabled() const;
    Q_INVOKABLE bool enableHotSpot() const;
    Q_INVOKABLE bool disableHotSpot() const;

private:
    Settings();
};

#endif // SETTINGS_H
