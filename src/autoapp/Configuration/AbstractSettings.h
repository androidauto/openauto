#ifndef ABSTRACTSETTINGS_H
#define ABSTRACTSETTINGS_H

#include <QSettings>
#include <QMutex>
#include <QMutexLocker>

#define SETTING(TYPE, NAME, DEFAULT_VALUE) \
    SETTING_PATH("", TYPE, NAME, DEFAULT_VALUE)

#define SETTING_PATH(PATH, TYPE, NAME, DEFAULT_VALUE)\
    Q_PROPERTY(TYPE NAME READ NAME WRITE set_##NAME NOTIFY NAME##_changed) \
Q_SIGNALS: \
    void NAME##_changed(TYPE value); \
public: \
    Q_INVOKABLE TYPE NAME() \
    {\
        QMutexLocker locker(&m_##NAME##_mutex); \
        return m_settings->value(QLatin1String(PATH"/"#NAME), DEFAULT_VALUE).value<TYPE>();\
    }\
    Q_INVOKABLE void set_##NAME(const TYPE & NAME) \
    {\
        QMutexLocker locker(&m_##NAME##_mutex); \
        m_settings->setValue(PATH"/"#NAME, NAME);\
        m_settings->sync();\
        locker.unlock();\
        emit NAME##_changed(NAME);\
    } \
private: \
    QMutex m_##NAME##_mutex;

#define SETTING_PATH_QOBJECT(PATH, NAME)\
public: \
    Q_INVOKABLE void read_##NAME(QObject *object) \
    {\
        auto propertyNames = QObjectHelper::propertyNames(object); \
        QMutexLocker locker(&m_##NAME##_mutex); \
        QVariant value; \
        for (const QString &propertyName : propertyNames) { \
            value = m_settings->value(QString(QLatin1String(PATH"/"#NAME"/%1")).arg(propertyName)); \
            if (!value.isValid()) { \
                continue; \
            } \
            object->setProperty(propertyName.toUtf8().constData(), value); \
        } \
    }\
    Q_INVOKABLE void save_##NAME(QObject *object) \
    {\
        auto propertyNames = QObjectHelper::propertyNames(object); \
        QMutexLocker locker(&m_##NAME##_mutex); \
        for (const QString &propertyName : propertyNames) { \
            m_settings->setValue(QString(QLatin1String(PATH"/"#NAME"/%1")).arg(propertyName), \
                                object->property(propertyName.toUtf8().constData()));\
        } \
        m_settings->sync();\
    } \
private: \
    QMutex m_##NAME##_mutex;

class AbstractSettings : public QObject
{
    Q_OBJECT

public:
    void init();

protected:
    AbstractSettings(const QString &filePath);
    QSettings *m_settings;
};

#endif // ABSTRACTSETTINGS_H
