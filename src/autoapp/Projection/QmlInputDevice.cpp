#include "QmlInputDevice.h"

#include <QCoreApplication>

#include <f1x/openauto/Common/Log.hpp>
#include <f1x/openauto/autoapp/Projection/IInputDeviceEventHandler.hpp>
#include <f1x/openauto/autoapp/Projection/InputDevice.hpp>

#include "f1x/openauto/autoapp/App.hpp"

#include <QDebug>

namespace f1x
{
namespace openauto
{
namespace autoapp
{
namespace projection
{

bool QmlInputDevice::mPaused = false;
App *QmlInputDevice::mApp = nullptr;

QmlInputDevice::QmlInputDevice(configuration::IConfiguration::Pointer configuration, const QRect& touchscreenGeometry, const QRect& displayGeometry)
    : mConfiguration(std::move(configuration))
    , mTouchscreenGeometry(touchscreenGeometry)
    , mDisplayGeometry(displayGeometry)
    , mEventHandler(nullptr)
{
    this->moveToThread(qApp->thread());

    connect(mApp, &App::mouseMove, this, &QmlInputDevice::handleMouseMove);
    connect(mApp, &App::mouseRelease, this, &QmlInputDevice::handleMouseRelease);
    connect(mApp, &App::mousePress, this, &QmlInputDevice::handleMousePress);
}

void QmlInputDevice::start(IInputDeviceEventHandler& eventHandler)
{
    QMutexLocker locker(&mMutex);
    mPaused = false;

    OPENAUTO_LOG(info) << "[QmlInputDevice] start.";
    mEventHandler = &eventHandler;
}

void QmlInputDevice::stop()
{
    QMutexLocker locker(&mMutex);

    OPENAUTO_LOG(info) << "[QmlInputDevice] stop.";
    mEventHandler = nullptr;
}


bool QmlInputDevice::hasTouchscreen() const
{
    return mConfiguration->getTouchscreenEnabled();
}

QRect QmlInputDevice::getTouchscreenGeometry() const
{
    return mTouchscreenGeometry;
}

void QmlInputDevice::pause()
{
    mPaused = true;
}

void QmlInputDevice::resume()
{
    mPaused = false;
}

void QmlInputDevice::setApp(App *app)
{
    mApp = app;
}

QPoint QmlInputDevice::mapMousePosition(int mouseX, int mouseY)
{
    const uint32_t x = (static_cast<float>(mouseX) / mTouchscreenGeometry.width()) * mDisplayGeometry.width();
    const uint32_t y = (static_cast<float>(mouseY) / mTouchscreenGeometry.height()) * mDisplayGeometry.height();
    return QPoint(x, y);
}

void QmlInputDevice::handleMousePress(int mouseX, int mouseY)
{
    handleTouchEvent(aasdk::proto::enums::TouchAction::PRESS, mouseX, mouseY);
}

void QmlInputDevice::handleMouseRelease(int mouseX, int mouseY)
{
    handleTouchEvent(aasdk::proto::enums::TouchAction::RELEASE, mouseX, mouseY);
}

void QmlInputDevice::handleMouseMove(int mouseX, int mouseY)
{
    handleTouchEvent(aasdk::proto::enums::TouchAction::DRAG, mouseX, mouseY);
}

void QmlInputDevice::handleTouchEvent(aasdk::proto::enums::TouchAction::Enum eventType, int mouseX, int mouseY)
{
    if (mPaused || mEventHandler == nullptr) {
        return;
    }

    qDebug() << __PRETTY_FUNCTION__ << eventType << mouseX << mouseY;

    const auto &mousePos = mapMousePosition(mouseX, mouseY);

    mEventHandler->onTouchEvent({eventType, (uint32_t)mousePos.x(), (uint32_t)mousePos.y(), 0});
}

IInputDevice::ButtonCodes QmlInputDevice::getSupportedButtonCodes() const
{
    return mConfiguration->getButtonCodes();
}
}
}
}
}
