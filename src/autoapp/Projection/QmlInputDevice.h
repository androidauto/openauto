#ifndef QMLINPUTDEVICE_H
#define QMLINPUTDEVICE_H

#include <QObject>
#include <QMutex>
#include <f1x/openauto/autoapp/Projection/IInputDevice.hpp>
#include <f1x/openauto/autoapp/Configuration/IConfiguration.hpp>

namespace f1x
{
namespace openauto
{
namespace autoapp
{
class App;
namespace projection
{
class QmlInputDevice: public QObject, public IInputDevice, boost::asio::noncopyable
{
    Q_OBJECT
public:
    QmlInputDevice(configuration::IConfiguration::Pointer configuration, const QRect& touchscreenGeometry, const QRect& videoGeometry);

    void start(IInputDeviceEventHandler& eventHandler) override;
    void stop() override;
    ButtonCodes getSupportedButtonCodes() const override;
    bool hasTouchscreen() const override;
    QRect getTouchscreenGeometry() const override;

    static void pause();
    static void resume();

    static void setApp(App *app);
private:
    QMutex mMutex;
    configuration::IConfiguration::Pointer mConfiguration;
    QRect mTouchscreenGeometry;
    QRect mDisplayGeometry;
    IInputDeviceEventHandler* mEventHandler = nullptr;

    static App *mApp;
    static bool mPaused;

    QPoint mapMousePosition(int mouseX, int mouseY);

    void handleMousePress(int mouseX, int mouseY);
    void handleMouseRelease(int mouseX, int mouseY);
    void handleMouseMove(int mouseX, int mouseY);

    void handleTouchEvent(aasdk::proto::enums::TouchAction::Enum eventType, int mouseX, int mouseY);
};


}
}
}
}


#endif // QMLINPUTDEVICE_H
