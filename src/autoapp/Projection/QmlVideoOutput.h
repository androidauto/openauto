#ifndef QMLVIDEOOUTPUT_H
#define QMLVIDEOOUTPUT_H

#include <boost/noncopyable.hpp>
#include <f1x/openauto/autoapp/Projection/VideoOutput.hpp>
#include <f1x/openauto/autoapp/Projection/SequentialBuffer.hpp>
#include <boost/asio/detail/noncopyable.hpp>

class QMediaPlayer;
class QVideoWidget;

namespace f1x
{
namespace openauto
{
namespace autoapp
{
namespace projection
{

class QmlVideoOutput: public QObject, public VideoOutput, boost::asio::noncopyable
{
    Q_OBJECT
public:
    QmlVideoOutput(configuration::IConfiguration::Pointer configuration);

    bool open() override;
    bool init() override;
    void write(uint64_t timestamp, const aasdk::common::DataConstBuffer& buffer) override;
    void stop() override;

    static void setMediaPlayer(QObject *mediaPlayer);

signals:
    void startPlayback();
    void stopPlayback();

protected slots:
    void onStartPlayback();
    void onStopPlayback();

private:
    SequentialBuffer mVideoBuffer;
    static QMediaPlayer *mMediaPlayer;
};

}
}
}
}

#endif // QMLVIDEOOUTPUT_H
