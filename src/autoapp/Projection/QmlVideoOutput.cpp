#include "QmlVideoOutput.h"

#include <QApplication>
#include <f1x/openauto/autoapp/Projection/QtVideoOutput.hpp>
#include <f1x/openauto/Common/Log.hpp>


namespace f1x
{
namespace openauto
{
namespace autoapp
{
namespace projection
{

QMediaPlayer *QmlVideoOutput::mMediaPlayer = nullptr;

QmlVideoOutput::QmlVideoOutput(configuration::IConfiguration::Pointer configuration)
    : VideoOutput(std::move(configuration))
{
    this->moveToThread(QApplication::instance()->thread());
    connect(this, &QmlVideoOutput::startPlayback, this, &QmlVideoOutput::onStartPlayback, Qt::QueuedConnection);
    connect(this, &QmlVideoOutput::stopPlayback, this, &QmlVideoOutput::onStopPlayback, Qt::QueuedConnection);
}

bool QmlVideoOutput::open()
{
    return mVideoBuffer.open(QIODevice::ReadWrite);
}

bool QmlVideoOutput::init()
{
    emit startPlayback();
    return true;
}

void QmlVideoOutput::stop()
{
    emit stopPlayback();
}

void QmlVideoOutput::setMediaPlayer(QObject *mediaPlayer)
{
    mMediaPlayer = qvariant_cast<QMediaPlayer *>(mediaPlayer->property("mediaObject"));
    qDebug() << mediaPlayer << mMediaPlayer;
}

void QmlVideoOutput::write(uint64_t, const aasdk::common::DataConstBuffer& buffer)
{
    mVideoBuffer.write(reinterpret_cast<const char*>(buffer.cdata), buffer.size);
}

void QmlVideoOutput::onStartPlayback()
{
    mMediaPlayer->setMedia(QMediaContent(), &mVideoBuffer);
    mMediaPlayer->play();
    OPENAUTO_LOG(debug) << "Player error state -> " << mMediaPlayer->errorString().toStdString();
}

void QmlVideoOutput::onStopPlayback()
{
    mMediaPlayer->stop();
}


}
}
}
}
