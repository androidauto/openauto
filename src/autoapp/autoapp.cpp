/*
*  This file is part of openauto project.
*  Copyright (C) 2018 f1x.studio (Michal Szwaj)
*
*  openauto is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 3 of the License, or
*  (at your option) any later version.

*  openauto is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with openauto. If not, see <http://www.gnu.org/licenses/>.
*/

#include <thread>
#include <QApplication>
#include <QDesktopWidget>
#include <QFontDatabase>
#include <QThread>
#include <f1x/aasdk/USB/USBHub.hpp>
#include <f1x/aasdk/USB/ConnectedAccessoriesEnumerator.hpp>
#include <f1x/aasdk/USB/AccessoryModeQueryChain.hpp>
#include <f1x/aasdk/USB/AccessoryModeQueryChainFactory.hpp>
#include <f1x/aasdk/USB/AccessoryModeQueryFactory.hpp>
#include <f1x/aasdk/TCP/TCPWrapper.hpp>
#include <f1x/openauto/autoapp/App.hpp>
#include <f1x/openauto/autoapp/Configuration/IConfiguration.hpp>
#include <f1x/openauto/autoapp/Configuration/RecentAddressesList.hpp>
#include <f1x/openauto/autoapp/Service/AndroidAutoEntityFactory.hpp>
#include <f1x/openauto/autoapp/Service/ServiceFactory.hpp>
#include <f1x/openauto/autoapp/Configuration/Configuration.hpp>
#include <f1x/openauto/autoapp/UI/MainWindow.hpp>
#include <f1x/openauto/autoapp/UI/SettingsWindow.hpp>
#include <f1x/openauto/autoapp/UI/ConnectDialog.hpp>
#include <f1x/openauto/autoapp/UI/WarningDialog.hpp>
#include <f1x/openauto/autoapp/UI/UpdateDialog.hpp>
#include <f1x/openauto/Common/Log.hpp>
#include <QtQml/QQmlApplicationEngine>
#include <QtQml/QQmlContext>
#include <autoapp/Helpers/ScriptHelper.h>
#include <autoapp/Projection/QmlInputDevice.h>

#include "Service/WifiMonitor.h"
#include "Configuration/Settings.h"
#include "Helpers/QmlProperties.h"

namespace aasdk = f1x::aasdk;
namespace autoapp = f1x::openauto::autoapp;
using ThreadPool = std::vector<std::thread>;

void startUSBWorkers(boost::asio::io_service& ioService, libusb_context* usbContext, ThreadPool& threadPool)
{
    auto usbWorker = [&ioService, usbContext]() {
        timeval libusbEventTimeout{3, 0};

        while(!ioService.stopped())
        {
            libusb_handle_events_timeout_completed(usbContext, &libusbEventTimeout, nullptr);
        }
    };

    threadPool.emplace_back(usbWorker);
}

void startIOServiceWorkers(boost::asio::io_service& ioService, ThreadPool& threadPool)
{
    auto ioServiceWorker = [&ioService]() {
        ioService.run();
    };

    threadPool.emplace_back(ioServiceWorker);
}

void initQml(std::shared_ptr<autoapp::App> app) {
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

#ifdef __arm__
    const bool isArm = true;
#else
    const bool isArm = false;
#endif

    autoapp::projection::QmlInputDevice::setApp(app.get());

    qDebug() << "Adding FontAwesomeRegular:" << QFontDatabase::addApplicationFont(":/Font Awesome 5 Free-Regular-400.otf");
    qDebug() << "Adding FontAwesomeSolid:" << QFontDatabase::addApplicationFont(":/Font Awesome 5 Free-Solid-900.otf");
    qDebug() << "Adding FontAwesomeBrands:" << QFontDatabase::addApplicationFont(":/Font Awesome 5 Brands-Regular-400.otf");

    qDebug() << "Adding tabler-icons:" << QFontDatabase::addApplicationFont(":/tabler-icons.ttf");

    auto *engine = new QQmlApplicationEngine(qApp);

    auto wifiMonitorThread = new QThread();
    WifiMonitor::instance()->moveToThread(wifiMonitorThread);
    wifiMonitorThread->start();

    engine->rootContext()->setContextProperty("properties", new QmlProperties(engine));
    engine->rootContext()->setContextProperty("settings", Settings::instance());
    engine->rootContext()->setContextProperty("wifiMonitor", WifiMonitor::instance());
    engine->rootContext()->setContextProperty("isArm", isArm);
    engine->rootContext()->setContextProperty("androidAutoApp", app.get());

    if (Settings::instance()->showCursor()) {
        qApp->setOverrideCursor(Qt::ArrowCursor);
    } else {
        qApp->setOverrideCursor(Qt::BlankCursor);
    }

    //delay a bit starting wifi
//    QTimer::singleShot(2000, [](){
//        if (ScriptHelper::hotSpotEnabled()) {
//            ScriptHelper::enableHotSpot();
//        }
//    });


    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(engine, &QQmlApplicationEngine::quit, &QGuiApplication::quit);
    QObject::connect(engine, &QQmlApplicationEngine::objectCreated, qApp, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine->load(url);
}

int main(int argc, char* argv[])
{
    libusb_context* usbContext;
    if(libusb_init(&usbContext) != 0)
    {
        OPENAUTO_LOG(error) << "[OpenAuto] libusb init failed.";
        return 1;
    }

    boost::asio::io_service ioService;
    boost::asio::io_service::work work(ioService);
    std::vector<std::thread> threadPool;
    startUSBWorkers(ioService, usbContext, threadPool);
    startIOServiceWorkers(ioService, threadPool);

    QApplication qApplication(argc, argv);
    const int width = QApplication::desktop()->width();
    const int height = QApplication::desktop()->height();
    OPENAUTO_LOG(info) << "[OpenAuto] Display width: " << width;
    OPENAUTO_LOG(info) << "[OpenAuto] Display height: " << height;

    auto configuration = std::make_shared<autoapp::configuration::Configuration>();

    autoapp::configuration::RecentAddressesList recentAddressesList(7);
    recentAddressesList.read();

    aasdk::tcp::TCPWrapper tcpWrapper;

    aasdk::usb::USBWrapper usbWrapper(usbContext);
    aasdk::usb::AccessoryModeQueryFactory queryFactory(usbWrapper, ioService);
    aasdk::usb::AccessoryModeQueryChainFactory queryChainFactory(usbWrapper, ioService, queryFactory);
    autoapp::service::ServiceFactory serviceFactory(ioService, configuration);
    autoapp::service::AndroidAutoEntityFactory androidAutoEntityFactory(ioService, configuration, serviceFactory);

    auto usbHub(std::make_shared<aasdk::usb::USBHub>(usbWrapper, ioService, queryChainFactory));
    auto connectedAccessoriesEnumerator(std::make_shared<aasdk::usb::ConnectedAccessoriesEnumerator>(usbWrapper, ioService, queryChainFactory));
    auto app = std::make_shared<autoapp::App>(ioService, usbWrapper, tcpWrapper, androidAutoEntityFactory, std::move(usbHub), std::move(connectedAccessoriesEnumerator));

    //QML
    WifiMonitor::instance()->setIoService(&ioService);
    WifiMonitor::instance()->setTcpWrapper(&tcpWrapper);
    QObject::connect(WifiMonitor::instance(), &WifiMonitor::hostFound, [&ioService, &tcpWrapper](const QString &host){
        qDebug() << Q_FUNC_INFO << "Host found:" << host;

        WifiMonitor::instance()->stop();
        WifiMonitor::instance()->connectToHost(host);
    });

    QObject::connect(WifiMonitor::instance(), &WifiMonitor::connectionFinished, [&app](auto socket){
        app->start(std::move(socket));
    });

    initQml(app);
    //QML

    app->waitForUSBDevice();

    auto result = qApplication.exec();

    try {
        app->stop();
    } catch (...) {
        OPENAUTO_LOG(error) << "[Autoapp] TriggerAppStop: stop();";
    }

    std::for_each(threadPool.begin(), threadPool.end(), std::bind(&std::thread::join, std::placeholders::_1));

    libusb_exit(usbContext);

    return result;
}

