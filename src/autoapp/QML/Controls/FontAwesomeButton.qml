import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.3

Button {
    font.pixelSize: 46
    font.family: properties.fontAwesome
    font.weight: Font.Black
    Material.background: "transparent"
}
