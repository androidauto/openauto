import QtQuick 2.11
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.0

Item {
    id: root

    opacity: 0
    visible: opacity != 0

    property alias title: titleObject.text
    property alias value: slider.value
    property alias min: slider.from
    property alias max: slider.to

    function close() {
        opacity = 0
    }

    function open() {
        opacity = 1
    }

    Behavior on opacity {
        NumberAnimation { duration: 200 }
    }

    Rectangle {
        anchors.fill: parent
        color: "#88000000"
    }

    MouseArea {
        anchors.fill: parent
        hoverEnabled: true
        onClicked: close()
    }

    Pane {
        anchors.centerIn: parent
        width: parent.width / 2
        height: parent.height / 2

        Material.elevation: 4

        Label {
            id: titleObject
            anchors {
                margins: 10
                top: parent.top
                horizontalCenter: parent.horizontalCenter
            }
        }

        Slider {
            id: slider
            anchors {
                left: parent.left
                right: parent.right
                verticalCenter: parent.verticalCenter
                margins: 10
            }
            stepSize: 1

            handle.implicitHeight: 40
            handle.implicitWidth: 40
        }

        Label {
            anchors {
                margins: 10
                top: slider.bottom
                horizontalCenter: parent.horizontalCenter
            }
            text: "%1%".arg(slider.value)
        }
    }
}
