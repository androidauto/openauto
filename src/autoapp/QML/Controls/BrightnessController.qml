import QtQuick 2.11
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.0

SliderDialog {
    id: root

    title: qsTr("Screen brightness")
    value: settings.brightness
    min: 10
    max: 100

    onValueChanged: {
        if (!visible)
            return
        settings.brightness = value
    }
}
