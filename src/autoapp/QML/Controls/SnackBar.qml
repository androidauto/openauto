import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.3

Rectangle {
    id: root

    readonly property color infoColor: Material.color(Material.Blue)
    readonly property color errorColor: Material.color(Material.Red)

    readonly property int timeout: 8000

    height: childrenRect.height + 20
    width: parent.width
    y: parent.height
    visible: false

    Behavior on y { NumberAnimation { duration: 200 } }

    function showInfo(text) {
        root.color = root.infoColor
        show(text)
    }

    function showError(text) {
        root.color = root.errorColor
        show(text)
    }

    function show(text) {
        visible = true
        label.text = text
        y = parent.height - height
        timer.start()
    }

    function hide() {
        y = parent.height
    }

    Timer {
        id: timer
        onTriggered: hide()
        interval: root.timeout
    }

    Label {
        id: label
        anchors {
            margins: 10
            left: parent.left
            right: parent.right
            verticalCenter: parent.verticalCenter
        }
        wrapMode: "WordWrap"
        color: "#FFFFFF"
        verticalAlignment: "AlignVCenter"
    }

    MouseArea {
        anchors.fill: parent
        onClicked: hide()
    }
}
