import QtQuick 2.11
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.0

SliderDialog {
    id: root

    title: qsTr("Volume")
    value: settings.volume
    min: 0
    max: 100

    onValueChanged: {
        if (!visible)
            return
        settings.volume = value
        androidAutoApp.setVolume(value)
    }
}
