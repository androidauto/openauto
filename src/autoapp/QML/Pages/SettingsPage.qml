import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.3
import QtQuick.Layouts 1.3

import "../Controls"

Page {
//    title: qsTr("Settings")
    title: qsTr("AMG Android Auto")

    Column {
        anchors.fill: parent
        spacing: 0

        RowLayout {
            height: parent.height / 2
            width: parent.width

            FontAwesomeButton {
                Layout.fillHeight: true
                Layout.fillWidth: true
                text: "\uf1eb"

                onClicked: stackView.push("WifiSettingsPage.qml")
            }

            FontAwesomeButton {
                Layout.fillHeight: true
                Layout.fillWidth: true
                text: "\uf294"
            }

            FontAwesomeButton {
                Layout.fillHeight: true
                Layout.fillWidth: true
                text: "\uf108"

//                onClicked: window.visibility = ApplicationWindow.Minimized
            }
        }

        RowLayout {
            height: parent.height / 2
            width: parent.width

            FontAwesomeButton {
                Layout.fillHeight: true
                Layout.fillWidth: true
                text: "\uf028"

                onClicked: volumeController.open()
            }

            FontAwesomeButton {
                Layout.fillHeight: true
                Layout.fillWidth: true
                text: "\uf042"

                onClicked: brightnessController.open()
            }

            FontAwesomeButton {
                Layout.fillHeight: true
                Layout.fillWidth: true
                text: "\uf011"

                Material.background: Material.color(Material.Red, Material.Shade900)
                onClicked: Qt.quit()
                onPressAndHold: androidAutoApp.reboot()
            }
        }
    }
}
