import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.3
import QtQuick.Layouts 1.3

import "../Controls"

BasePage {
    title: qsTr("Add WiFi host")

    actions: [ saveButton ]

    function save() {
        var wifiDevices = settings.wifiDevices
        if (wifiDevices.indexOf(hostField.text) !== -1) {
            snackBar.showError(qsTr("IP already on a list"))
            return
        }

        wifiDevices.push(hostField.text)
        settings.wifiDevices = wifiDevices

        stackView.pop()
    }

    Component {
        id: saveButton

        FontAwesomeButton {
            Layout.alignment: Qt.AlignVCenter
            text: "\uf00c"
            onClicked: save()
        }
    }

    ColumnLayout {
        anchors {
            margins: 10
            fill: parent
        }

        Label {
            text: qsTr("Host:")
        }

        TextField {
            id: hostField
            Layout.fillWidth: true
        }

        Item {
            Layout.fillWidth: true
            Layout.fillHeight: true
        }
    }
}
