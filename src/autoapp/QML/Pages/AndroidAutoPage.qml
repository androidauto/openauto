import QtQuick 2.11
import QtQuick.Controls.Material 2.3
import QtMultimedia 5.9

Rectangle {
    id: root
//    title: qsTr("AMG Android Auto")
    color: Material.color(Material.Grey, Material.Shade900)
    visible: false

    property bool connected: false

    function show() {
        if (!mediaplayer.hasVideo)
            return

        visible = true
    }

    function hide() {
        visible = false
    }

    Component.onCompleted: {
        androidAutoApp.setMediaPlayer(mediaplayer)
    }

    MediaPlayer {
        id: mediaplayer

        onPlaying: {
            root.connected = true
            visible = true
        }
        onStopped: {
            root.connected = false
            hide()
        }
    }

    VideoOutput {
        id: videoOutput
        anchors.fill: parent
        source: mediaplayer
        fillMode: VideoOutput.Stretch
    }

    MouseArea {
        anchors.fill: parent

        acceptedButtons: "LeftButton"

        onPressed: androidAutoApp.mousePress(mouse.x, mouse.y)
        onReleased: androidAutoApp.mouseRelease(mouse.x, mouse.y)
        onMouseXChanged: androidAutoApp.mouseMove(mouse.x, mouse.y)
        onMouseYChanged: androidAutoApp.mouseMove(mouse.x, mouse.y)
    }
}
