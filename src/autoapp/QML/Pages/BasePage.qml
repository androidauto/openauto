import QtQuick 2.11
import QtQuick.Controls 2.4

Page {
    property var actions: []
    property var buttons: []

    Component.onCompleted: {
        var tmp = buttons
        for (var i = 0 ; i < actions.length ; ++i) {
            tmp.push(actions[i].createObject(toolBarLayout))
        }
        buttons = tmp
    }

    Component.onDestruction: {
        for (var i = 0 ; i < buttons.length ; ++i) {
            buttons[i].destroy()
        }
    }

    onVisibleChanged: {
        for (var i = 0 ; i < buttons.length ; ++i) {
            buttons[i].visible = visible
        }
    }
}
