import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.3
import QtQuick.Layouts 1.3

import "../Controls"

BasePage {
    id: root
    title: qsTr("WiFi Settings")

    property bool initialized: false
    actions: [ addButton ]

    onVisibleChanged: {
        if (!visible) return

        refresh()
    }

    Component.onCompleted: {
        hotSpotButton.checked = settings.hotSpotEnabled()
        autoConnectCheckbox.checked = settings.wifiAutoconnect
        refresh()
        initialized = true
    }

    function refresh() {
        devicesListView.model = settings.wifiDevices
    }

    function remove(host) {
        var tmp = settings.wifiDevices
        var idx = tmp.indexOf(host)
        if (idx === -1) {
            console.warn("Host %1 not found".arg(host))
            return
        }

        tmp.splice(idx, 1)

        settings.wifiDevices = tmp
        refresh()
    }

    function connect(host) {
        wifiMonitor.connectToHost(host)
    }

    Component {
        id: addButton

        FontAwesomeButton {
            Layout.alignment: Qt.AlignVCenter
            text: "\uf067"
            onClicked: stackView.push("WifiAddHostPage.qml")
        }
    }

    ColumnLayout {
        anchors {
            fill: parent
            margins: 10
        }

        Row {
            Layout.preferredHeight: 100
            Layout.fillWidth: true

            spacing: 20

            Button {
                id: autoConnectCheckbox
                Material.accent: Material.color(Material.Red)
                Material.background: "transparent"
                checkable: true
                text: qsTr("Autoconnect")
                height: parent.height

                onCheckedChanged: {
                    if (!initialized) return

                    settings.wifiAutoconnect = checked
                }
            }

            Button {
                id: hotSpotButton
                Material.accent: Material.color(Material.Red)
                Material.background: "transparent"
                checkable: true
                text: checked ? qsTr("Disable HotSpot") : qsTr("Enable HotSpot")
                height: parent.height

                onCheckedChanged: {
                    if (!initialized) {
                        return
                    }

                    if (checked) {
                        settings.enableHotSpot()
                    } else {
                        settings.disableHotSpot()
                    }
                }
            }
        }

//        CheckBox {
//            id: autoConnectCheckbox
//            Layout.fillWidth: true
//            text: qsTr("Autoconnect")

//            onCheckedChanged: {
//                if (!initialized) return

//                settings.wifiAutoconnect = checked
//            }
//        }

        Label {
            text: qsTr("Known devices:")
        }

        Rectangle {
            Layout.fillWidth: true
            Layout.preferredHeight: 1
            color: Material.color(Material.Grey)
        }

        ListView {
            id: devicesListView
            Layout.fillHeight: true
            Layout.fillWidth: true

            clip: true
            boundsBehavior: ListView.StopAtBounds

            delegate: ItemDelegate {
                text: modelData
                width: parent.width
                height: 60


                FontAwesomeButton {
                    anchors.right: removeButton.left
                    height: parent.height
                    width: height
                    text: "\uf00c"
                    onClicked: root.connect(modelData)
                }

                FontAwesomeButton {
                    id: removeButton
                    anchors.right: parent.right
                    height: parent.height
                    width: height
                    text: "\uf068"
                    onClicked: root.remove(modelData)
                }
            }
        }
    }
}
