import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.3
import QtQuick.Layouts 1.3

import "Controls"
import "Pages"

ApplicationWindow {
    id: window
    visible: true
    width: 1024
    height: 600
    title: qsTr("Stack")

    visibility: isArm ? ApplicationWindow.FullScreen : ApplicationWindow.Windowed

    Material.accent: Material.color(Material.Grey, Material.Shade900)
    Material.primary: Material.color(Material.Grey, Material.Shade900)
    Material.foreground:  Material.color(Material.Grey, Material.Shade400)

    Component.onCompleted: {

    }

    function runDelayed(interval, callbackFunction) {
        var timer = Qt.createQmlObject("import QtQuick 2.11; Timer {}", window);
        timer.interval = interval;
        timer.repeat = false;
        timer.triggered.connect(callbackFunction)
        timer.start();
    }

    Connections {
        target: androidAutoApp
        onAndroidAutoExitClicked: androidAutoPage.hide()
    }

    header: ToolBar {
        contentHeight: 70

        RowLayout {
            id: toolBarLayout
            anchors.fill: parent

            FontAwesomeButton {
                id: backButton
                visible: stackView.depth > 1
                text: stackView.depth > 1 ? "\uf060" : "\uf0c9"
                onClicked: {
                    if (stackView.depth > 1) {
                        stackView.pop()
                    } else {
                        drawer.open()
                    }
                }
            }

            Label {
                text: stackView.currentItem.title
                font.pixelSize: 30
                Layout.fillWidth: true
                horizontalAlignment: "AlignLeft"
                padding: 10
            }

            FontAwesomeButton {
                Layout.alignment: Qt.AlignVCenter
                text: "\uf17b"
                onClicked: androidAutoPage.show()
                visible: androidAutoPage.connected
            }
        }
    }

//    Drawer {
//        id: drawer
//        width: window.width * 0.66
//        height: window.height

//        Column {
//            anchors.fill: parent

//            ItemDelegate {
//                text: qsTr("Android Auto")
//                width: parent.width
//                onClicked: {
//                    stackView.push("Pages/HomePage.qml")
//                    drawer.close()
//                }
//            }
//            ItemDelegate {
//                text: qsTr("A2DP")
//                width: parent.width
//                onClicked: {
//                    stackView.push("Pages/HomePage.qml")
//                    drawer.close()
//                }
//            }
//        }
//    }

    StackView {
        id: stackView
//        initialItem: "Pages/HomePage.qml"
        initialItem: "Pages/SettingsPage.qml"
        anchors.fill: parent
    }

    AndroidAutoPage {
        id: androidAutoPage
        anchors {
            topMargin: -header.height
            fill: parent
        }
        z: 99
    }

    SnackBar {
        id: snackBar
    }

    BrightnessController {
        id: brightnessController
        anchors {
            topMargin: -header.height
            fill: parent
        }
        z: 100
    }

    VolumeController {
        id: volumeController
        anchors {
            topMargin: -header.height
            fill: parent
        }
        z: 100
    }

    Rectangle {
        z: 999
        anchors {
            topMargin: -header.height
            fill: parent
        }
        color: "#000000"
        opacity: (100-settings.brightness) / 100
    }
}
