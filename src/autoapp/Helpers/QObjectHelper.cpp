#include "QObjectHelper.h"

#include <QObject>
#include <QDebug>
#include <QMetaProperty>
#include <QQmlEngine>

QStringList QObjectHelper::propertyNames(const QObject *object)
{
    QStringList result;
    if (object == nullptr) {
        qWarning() << "Object can't be null";
        return result;
    }

    const QMetaObject *meta = object->metaObject();
    for (int i = 0 ; i < meta->propertyCount(); ++i) {
        const QMetaProperty property = meta->property(i);
        const QString propertyName = property.name();
        if (propertyName == "objectName") {
            continue;
        }
        result << propertyName;
    }

    return result;
}

int QObjectHelper::propertyCount(const QObject *object)
{
    return object->metaObject()->propertyCount() - 1; //we have to remove 'objectName'
}

QVariantHash QObjectHelper::properties(const QObject *object)
{
    QVariantHash result;
    if (object == nullptr) {
        qWarning() << "Object can't be null";
        return result;
    }

    const QMetaObject *meta = object->metaObject();
    for (int i = 0 ; i < meta->propertyCount(); ++i) {
        const QMetaProperty property = meta->property(i);
        const QString propertyName = property.name();
        if (propertyName == "objectName") {
            continue;
        }
        result.insert(propertyName, property.read(object));
    }

    return result;
}

QVariant QObjectHelper::propertyValue(const QObject *object, int propertyId)
{
    const QMetaObject *meta = object->metaObject();
    return meta->property(propertyId + 1).read(object);
}

bool QObjectHelper::reassign(const QObject *source, QObject *target)
{
    if (source == nullptr || target == nullptr) {
        qWarning() << "source nor target can be null";
        return false;
    }

    const QMetaObject *meta = source->metaObject();
    for (int i = 0 ; i < meta->propertyCount(); ++i) {
        const QMetaProperty property = meta->property(i);
        const QString propertyName = property.name();
        if (propertyName == "objectName") {
            continue;
        }

        property.write(target, property.read(source));
    }

    return true;
}

bool QObjectHelper::reassign(const QVector<QVariant> &initializerList, QObject *target)
{
    if (initializerList.size() != propertyCount(target)) {
        qWarning() << "property count doesn't match" << initializerList.size() << propertyCount(target);
        return false;
    }

    const QMetaObject *meta = target->metaObject();
    for (int i = 1 ; i < meta->propertyCount(); ++i) { //from 1 because first is objectName
        const QMetaProperty property = meta->property(i);
        const QString propertyName = property.name();

        if (!property.write(target, initializerList.at(i - 1))) { //1st is objectName
            return false;
        }
    }

    return true;
}

bool QObjectHelper::equal(const QObject *one, const QObject *other)
{
    if (one == nullptr || other == nullptr) {
        qWarning() << "source nor target can be null";
        return false;
    }

    const QMetaObject *meta = one->metaObject();
    for (int i = 0 ; i < meta->propertyCount(); ++i) {
        const QMetaProperty property = meta->property(i);
        const QString propertyName = property.name();
        if (propertyName == "objectName") {
            continue;
        }

        if (property.read(one) != property.read(other)) {
            return false;
        }
    }

    return true;
}

void QObjectHelper::ownByCpp(QObject *object)
{
    QQmlEngine::setObjectOwnership(object, QQmlEngine::CppOwnership);
}

void QObjectHelper::ownByQml(QObject *object)
{
    QQmlEngine::setObjectOwnership(object, QQmlEngine::JavaScriptOwnership);
}
