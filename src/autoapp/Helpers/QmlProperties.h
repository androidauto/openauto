#ifndef QMLPROPERTIES_H
#define QMLPROPERTIES_H

#include <QObject>

#include "PropertyMacros.h"

class QmlProperties : public QObject {
    Q_OBJECT

    PROPERTY_DEFAULT(QString, fontAwesome, "Font Awesome 5 Free")
public:
    QmlProperties(QObject *parent = 0) : QObject(parent) {}
};

#endif // QMLPROPERTIES_H
