#ifndef SCRIPTHELPER_H
#define SCRIPTHELPER_H

#define SCRIPT_PATH "/opt/motico/scripts/"
#define WIFI_SCRIPT_PATH "/opt/motico/scripts/wifi/"

class ScriptHelper
{
public:
    ScriptHelper();

    static bool enableHotSpot();
    static bool disableHotSpot();
    static bool hotSpotEnabled();

    static bool reboot();
};

#endif // SCRIPTHELPER_H
