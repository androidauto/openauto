#ifndef QOBJECTHELPER_H
#define QOBJECTHELPER_H

#include <QStringList>
#include <QVariantHash>

#include <QObject>
#include <QVector>

class QObjectHelper
{
public:
    /*!
     * \brief propertyNames - helper method for getting propertyNames list from an object
     * \param object
     * \return
     */
    static QStringList propertyNames(const QObject *object);

    /*!
     * \brief propertyCount - helper method for getting property count from an object
     * \param object
     * \return
     */
    static int propertyCount(const QObject *object);

    /*!
     * \brief properties - helper method for getting property names and values from an object
     * \param object
     * \return
     */
    static QVariantHash properties(const QObject *object);

    /*!
     * \brief propertyValue - returns property value with idx=propertyId from an object
     * \param object
     * \param propertyId
     * \return
     */
    static QVariant propertyValue(const QObject *object, int propertyId);

    /*!
     * \brief reassign - reassigns properties from source to target. We can consider this as operator=
     * \param source
     * \param target
     * \return
     */
    static bool reassign(const QObject *source, QObject *target);

    /*!
     * \brief reassign - reassigns properties from QVector to a target. it can be used inside initializer list constructor
     * \param initializerList
     * \return
     */
    static bool reassign(const QVector<QVariant> &initializerList, QObject *target);

    /*!
     * \brief equal - compares all properties from both objects and returns true if they all match
     * if any of the properties will be different, then method will return false.
     * It's allowed to compare different QObject types although it's not advised to do so
     * \param one
     * \param other
     * \return
     */
    static bool equal(const QObject *one, const QObject *other);

    /*!
     * \brief ownByCpp - helper method to tell QML engine that it shouldn't take ownership of the object and that CPP will handle disposing.
     * This method should be used e.g. when getting some object from CPP into QML directly: someManager.getSomeObject().doSomethingOnTheObject().
     * In this situation, object returned by getSomeObject() will be disposed when GC will process this pointer
     * \param object
     */
    void static ownByCpp(QObject *object);

    /*!
     * \brief ownByQml - helper method to tell QML engine that it should take ownership of the object and that it should dispose it whenever it's suitable
     * \param object
     */
    void static ownByQml(QObject *object);

    template <class T>
    int static indexOf(const T &container, const QString &propertyName, const QVariant &propertyValue) {
        for (int i = 0 ; i < container.count() ; ++i) {
            if (container.at(i).property(propertyName.toUtf8().constData()) == propertyValue) {
                return i;
            }
        }
        return -1;
    }
};

#endif // QOBJECTHELPER_H
