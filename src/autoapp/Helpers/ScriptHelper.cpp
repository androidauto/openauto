#include "ScriptHelper.h"

#include <QProcess>

ScriptHelper::ScriptHelper()
{

}

bool ScriptHelper::enableHotSpot()
{
    return QProcess::execute(WIFI_SCRIPT_PATH "enable_hotspot.sh") == 0;
}

bool ScriptHelper::disableHotSpot()
{
    return QProcess::execute(WIFI_SCRIPT_PATH "disable_hotspot.sh") == 0;
}

bool ScriptHelper::hotSpotEnabled()
{
    return QProcess::execute(WIFI_SCRIPT_PATH "hotspot_enabled.sh") == 0;
}

bool ScriptHelper::reboot()
{
    return QProcess::execute("sudo reboot");
}
