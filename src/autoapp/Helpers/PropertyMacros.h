#ifndef PROPERTYMACROS_H
#define PROPERTYMACROS_H

#include <QVariant>
//#include <Database/SqlWorker.h>

/**************** Regular Property ****************/

#define GETTER_SETTER_DEFAULT(TYPE, NAME, DEFAULT) \
    private: \
        TYPE m_##NAME = DEFAULT; \
        bool m_##NAME##_dirty = false; \
    public: \
        TYPE NAME() const \
        { \
            return m_##NAME; \
        } \
        void set_##NAME(const TYPE & NAME) \
        { \
            m_##NAME = NAME; \
            m_##NAME##_dirty = true; \
            emit NAME##_changed(m_##NAME); \
        } \
        bool NAME##_dirty() const \
        { \
            return m_##NAME##_dirty; \
        } \
    Q_SIGNALS: \
        void NAME##_changed(TYPE value); \
    private:

#define GETTER_SETTER(TYPE, NAME) \
    GETTER_SETTER_DEFAULT(TYPE, NAME, QVariant().value<TYPE>())

#define PROPERTY_DEFAULT(TYPE, NAME, DEFAULT) \
private: \
    Q_PROPERTY(TYPE NAME READ NAME WRITE set_##NAME NOTIFY NAME##_changed) \
    GETTER_SETTER_DEFAULT(TYPE, NAME, DEFAULT) \

#define PROPERTY(TYPE, NAME) \
    PROPERTY_DEFAULT(TYPE, NAME, QVariant().value<TYPE>())


/**************** Regular Property ****************/


/**************** Pointer Property ****************/

#define GETTER_SETTER_POINTER_DEFAULT(TYPE, NAME, DEFAULT) \
    private: \
        TYPE m_##NAME = DEFAULT; \
    bool m_##NAME##_dirty = false; \
    public: \
        TYPE NAME() const \
        { \
            return m_##NAME; \
        } \
        void set_##NAME(TYPE NAME) \
        { \
            m_##NAME = NAME; \
            m_##NAME##_dirty = true; \
            emit NAME##_changed(m_##NAME); \
        } \
        bool NAME##_dirty() const \
        { \
            return m_##NAME##_dirty; \
        } \
    Q_SIGNALS: \
        void NAME##_changed(TYPE value); \
    private:

#define GETTER_SETTER_POINTER(TYPE, NAME) \
    GETTER_SETTER_POINTER_DEFAULT(TYPE, NAME, nullptr)

#define PROPERTY_POINTER_DEFAULT(TYPE, NAME, DEFAULT) \
private: \
    Q_PROPERTY(TYPE NAME READ NAME WRITE set_##NAME NOTIFY NAME##_changed) \
    GETTER_SETTER_POINTER_DEFAULT(TYPE, NAME, DEFAULT)

#define PROPERTY_POINTER(TYPE, NAME) \
    PROPERTY_POINTER_DEFAULT(TYPE, NAME, nullptr)

/**************** Regular Property ****************/

/**************** Mutexed Property ****************/

#define GETTER_SETTER_DEFAULT_MUTEXED(TYPE, NAME, DEFAULT) \
    private: \
        QMutex m_##NAME##_mutex; \
        TYPE m_##NAME = DEFAULT; \
        bool m_##NAME##_dirty = false; \
    public: \
        TYPE NAME() \
        { \
            QMutexLocker locker(&m_##NAME##_mutex); \
            return m_##NAME; \
        } \
        void set_##NAME(const TYPE & NAME) \
        { \
            QMutexLocker locker(&m_##NAME##_mutex); \
            m_##NAME = NAME; \
            m_##NAME##_dirty = true; \
            locker.unlock(); \
            emit NAME##_changed(NAME); \
        } \
        bool NAME##_dirty() \
        { \
            QMutexLocker locker(&m_##NAME##_mutex); \
            return m_##NAME##_dirty; \
        } \
    Q_SIGNALS: \
        void NAME##_changed(TYPE value); \
    private:


/**************** Mutexed Property ****************/

#define QOBJECT_INITIALIZER_LIST_CONSTRUCTOR(TYPE) \
public: \
    TYPE (QVector<QVariant> initializerList) { \
        QObjectHelper::reassign(initializerList, this); \
    }
/*!
  QObject based classes require copy constructor and assign operator.
  Without it you can't make containers with values (stack), you would have to use pointers(heap)
*/
#define DEFAULT_CONSTRUCTORS_AND_OPERATORS(TYPE) \
    public: \
    TYPE (QObject *parent = nullptr) : QObject(parent) {} \
    \
    TYPE (const TYPE &other, QObject *parent = nullptr) : QObject(parent) \
    { \
        *this = other; \
    } \
    \
    TYPE* operator =(const TYPE &other) \
    { \
        QObjectHelper::reassign(&other, this); \
        return this; \
    } \
    bool operator ==(const TYPE &other) const \
    { \
        return QObjectHelper::equal(this, &other); \
    } \
    bool operator !=(const TYPE &other) const \
    { \
        return !QObjectHelper::equal(this, &other); \
    }

/*!
  Adds base features for any Dictionary based entity
  When adding a new Dictionary entity, ensure that registerMeta() is called in BaseTypes::registerDictionaryBaseTypes()
*/
#define DICT_ENTITY_CONSTRUCTORS_AND_METHODS(TYPE, ENUM) \
    public: \
    TYPE (QObject *parent = nullptr) : IDNameEntity(parent) {} \
    \
    TYPE (const TYPE &other, QObject *parent = nullptr) : IDNameEntity(parent) \
    { \
        *this = other; \
    } \
    \
    TYPE* operator =(const TYPE &other) \
    { \
        QObjectHelper::reassign(&other, this); \
        return this; \
    } \
    \
    static void registerEnumMetaType() { \
    \
        qRegisterMetaType<ENUM>(#ENUM); \
    } \
    QOBJECT_INITIALIZER_LIST_CONSTRUCTOR(TYPE)


#define DICT_ENTITY_GET_NAME_METHOD(TYPE, KEY) \
    public: \
    QString getName(KEY key) \
    { \
        auto nameValues = names(); \
        if(!nameValues.contains(key)) { \
            qDebug() << "Failed to find name for key: " << QMetaEnum::fromType<KEY>().valueToKey(key); \
            return QString(); \
        } \
        return nameValues.value(key); \
    } \
/*!

  Adds base features (tableName, primaryKey etc) to the SQL entity classes stored inside BaseTypes

  */
#define BASE_ENTITY_METHODS(TYPE,TABLE_NAME,PRIMARY_KEY) \
    public: \
    static QString tableName() \
    { \
      return TABLE_NAME;\
    } \
    \
    static QString primaryKey() \
    { \
      return PRIMARY_KEY;\
    } \
    \
    bool saveInSql() const \
    { \
        SqlWorker worker; \
        return worker.insert<TYPE>(*this); \
    } \

#define SINGLETON(TYPE) \
    static TYPE* instance() { \
        static TYPE *mInstance = new TYPE(); \
        return mInstance; \
    }

#define SINGLETON_BY_VALUE(TYPE) \
    static TYPE* instance() { \
        static TYPE mInstance; \
        return &mInstance; \
    }


#endif // PROPERTYMACROS_H
