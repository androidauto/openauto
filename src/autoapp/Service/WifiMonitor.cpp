#include "WifiMonitor.h"

#include <QDebug>
#include <QProcess>

#include <autoapp/Configuration/Settings.h>

WifiMonitor::WifiMonitor() : QObject()
{
    connect(Settings::instance(), &Settings::wifiAutoconnect_changed, [=](){
        if (Settings::instance()->wifiAutoconnect()) {
            start();
        } else {
            stop();
        }
    });

    mTimer.setInterval(m_interval);
    connect(&mTimer, &QTimer::timeout, this, &WifiMonitor::scan);

    if (Settings::instance()->wifiAutoconnect()) {
//        QTimer::singleShot(1, this, &WifiMonitor::scan);
        start();
    }
}

void WifiMonitor::start()
{
    qDebug() << Q_FUNC_INFO;
    mTimer.start();
}

void WifiMonitor::stop()
{
    qDebug() << Q_FUNC_INFO;
    mTimer.stop();
}

void WifiMonitor::connectToHost(const QString &host)
{
    qDebug() << Q_FUNC_INFO << "Connect to host:" << host;
    const auto &ipAddress = host.toStdString();
    auto socket = std::make_shared<boost::asio::ip::tcp::socket>(*mIoService);
    mTcpWrapper->asyncConnect(*socket, ipAddress, 5277, std::bind(&WifiMonitor::connectHandler, this, std::placeholders::_1, ipAddress, socket));
}

void WifiMonitor::setIoService(boost::asio::io_service *ioService)
{
    mIoService = ioService;
}

void WifiMonitor::setTcpWrapper(f1x::aasdk::tcp::ITCPWrapper *tcpWarpper)
{
    mTcpWrapper = tcpWarpper;
}

void WifiMonitor::scan()
{
    qDebug() << Q_FUNC_INFO;
    auto hosts = Settings::instance()->wifiDevices();
    if (hosts.isEmpty()) {
        qDebug() << Q_FUNC_INFO << "no hosts to scan";
        return;
    }

    for (const QString &host : hosts) {
        QProcess process;
        process.start("/usr/bin/ping", { "-c 1", host });
        process.waitForStarted(3000);
        process.waitForFinished(-1);
        if (process.exitCode() == 0) {
            qDebug() << "host online" << host;
            emit hostFound(host);
            return;
        } else {
            qDebug() << "host offline:" << host;
        }
    }
}

void WifiMonitor::connectHandler(const boost::system::error_code& ec, const std::string& ipAddress, const f1x::aasdk::tcp::ITCPEndpoint::SocketPointer &socket)
{
//    qDebug() << Q_FUNC_INFO << ec.failed() << ec.value();
    if(!ec) {
        emit connectionFinished(std::move(socket), ipAddress);
    } else {
        emit connectionFailed(QString::fromStdString(ec.message()));
    }
}
