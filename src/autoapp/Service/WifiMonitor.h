#ifndef WIFIMONITOR_H
#define WIFIMONITOR_H

#include "../Helpers/PropertyMacros.h"

#include <QTimer>

#include "boost/system/error_code.hpp"
#include "f1x/aasdk/TCP/ITCPEndpoint.hpp"
#include "f1x/aasdk/TCP/ITCPWrapper.hpp"

class QMediaPlayer;
class QVideoWidget;

class WifiMonitor : public QObject
{
    Q_OBJECT

    PROPERTY_DEFAULT(int, interval, 5000)

    WifiMonitor();
public:
    SINGLETON_BY_VALUE(WifiMonitor)

    Q_INVOKABLE void start();
    Q_INVOKABLE void stop();

    Q_INVOKABLE void connectToHost(const QString &host);

    void setIoService(boost::asio::io_service *ioService);
    void setTcpWrapper(f1x::aasdk::tcp::ITCPWrapper *tcpWarpper);

private:
    QTimer mTimer;

    boost::asio::io_service *mIoService = nullptr;
    f1x::aasdk::tcp::ITCPWrapper *mTcpWrapper = nullptr;

    void scan();

    void connectHandler(const boost::system::error_code& ec, const std::string& ipAddress, const f1x::aasdk::tcp::ITCPEndpoint::SocketPointer &socket);

signals:
    void hostFound(const QString &host) const;

    void connectionFinished(f1x::aasdk::tcp::ITCPEndpoint::SocketPointer socket, const std::string& ipAddress);
    void connectionFailed(const QString &error);

};

#endif // WIFIMONITOR_H
