#!/bin/bash

PWD="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

if $PWD/hotspot_enabled.sh ; then
	$PWD/enable_hotspot/hostapdstart >1&
	exit 0
fi

exit 0