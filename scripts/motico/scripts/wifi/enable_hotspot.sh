#!/bin/bash

PWD="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

cp $PWD/enable_hotspot/dnsmasq.conf /etc/
cp $PWD/enable_hotspot/hostapd.conf /etc/hostapd/
cp $PWD/enable_hotspot/interfaces /etc/network/

$PWD/enable_hotspot/hostapdstart >1&
