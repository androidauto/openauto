#!/bin/bash

PWD="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

cp $PWD/disable_hotspot/dnsmasq.conf /etc/
rm /etc/hostapd/hostapd.conf
cp $PWD//disable_hotspot/interfaces /etc/network/

$PWD//disable_hotspot/hostapdstop
